﻿//-----------------------------------------------------------------------
// <copyright file="Enums.cs" company="Castores Inc.">
//     Custom company copyright tag.
// </copyright>
//-----------------------------------------------------------------------
[module: System.Diagnostics.CodeAnalysis.SuppressMessage("StyleCop.CSharp.DocumentationRules", "SA1649:FileHeaderFileNameDocumentationMustMatchTypeName", Justification = "Reviewed.")]

namespace FontCreator
{
    /// <summary>
    /// ExportType enumeration
    /// </summary>
    public enum ExportType
    {
        /// <summary>
        /// The esp8266
        /// </summary>
        Esp8266
    }

    /// <summary>
    /// PixelType enumeration
    /// </summary>
    public enum PixelType
    {
        /// <summary>
        /// The none
        /// </summary>
        None,

        /// <summary>
        /// The foreground
        /// </summary>
        Foreground,

        /// <summary>
        /// The background
        /// </summary>
        Background
    }
}
