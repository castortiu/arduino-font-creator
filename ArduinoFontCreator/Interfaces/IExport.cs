﻿//-----------------------------------------------------------------------
// <copyright file="IExport.cs" company="Castores Inc.">
//     Custom company copyright tag.
// </copyright>
//-----------------------------------------------------------------------
namespace FontCreator
{
    using System.Collections.Generic;

    /// <summary>
    /// IExport interface
    /// </summary>
    public interface IExport
    {
        /// <summary>
        /// Exports the specified file name.
        /// </summary>
        /// <param name="fileName">Name of the file.</param>
        /// <param name="glyphs">The glyphs.</param>
        /// <param name="legacy">Export as Adafruit format.</param>
        /// <returns>True if the export was successful</returns>
        bool Export(string fileName, IList<Glyph> glyphs, bool legacy);
    }
}
