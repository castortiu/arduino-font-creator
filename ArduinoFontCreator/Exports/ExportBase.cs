﻿//-----------------------------------------------------------------------
// <copyright file="ExportBase.cs" company="Castores Inc.">
//     Custom company copyright tag.
// </copyright>
//-----------------------------------------------------------------------
namespace FontCreator
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Windows.Controls;
    using System.Windows.Media;
    using System.Windows.Shapes;
    using Path = System.IO.Path;

    /// <inheritdoc />
    public abstract class ExportBase : IExport
    {
        /// <summary>
        /// Gets the addresses.
        /// </summary>
        public List<ExportGlyphRecord> Addresses { get; private set; }

        /// <summary>
        /// Gets the bitmaps.
        /// </summary>
        public List<byte> Bitmaps { get; private set; }

        /// <summary>
        /// Gets the filename.
        /// </summary>
        public string Filename { get; private set; }

        /// <inheritdoc />
        public bool Export(string fileName, IList<Glyph> glyphs, bool legacy)
        {
            Filename = Path.GetFileNameWithoutExtension(fileName);

            if (Filename == null)
            {
                return false;
            }

            // Convert first character to lower case
            Filename = char.ToLower(Filename[0]) + (Filename.Length > 1 ? Filename.Substring(1) : string.Empty);

            Addresses = new List<ExportGlyphRecord>();
            Bitmaps = new List<byte>();

            using (StreamWriter streamWriter = new StreamWriter(fileName, false))
            {
                Export(glyphs, streamWriter, legacy);
                return true;
            }
        }

        /// <summary>
        /// Exports the specified glyphs.
        /// </summary>
        /// <param name="glyphs">The glyphs.</param>
        /// <param name="writer">The writer.</param>
        /// <param name="legacy">if set to <c>true</c> [legacy].</param>
        protected void Export(IList<Glyph> glyphs, StreamWriter writer, bool legacy)
        {
            if (glyphs == null)
            {
                return;
            }

            object[] parameters =
            {
                legacy ? 8 : 4,
                legacy ? "GFX" : "XT",
                legacy ? 0b00000001 : 0b00000011
            };

            int first = -1;
            int last = -1;
            int yAdvance = -1;
            foreach (Glyph glyph in glyphs.OrderBy(_ => _.Ascii))
            {
                if (glyph.Used == false)
                {
                    if (first != -1)
                    {
                        // Add empty record
                        ExportGlyphRecord emptyRecord = new ExportGlyphRecord
                        {
                            Glyph = new Glyph(null)
                            {
                                Ascii = glyph.Ascii,
                                Name = glyph.Name,
                                Width = 0,
                                Height = 0,
                                AdvanceX = 0,
                                OffsetY = 0,
                                OffsetX = 0
                            },
                            BitmapOffset = 0
                        };
                        Addresses.Add(emptyRecord);
                    }

                    continue;
                }

                if (first == -1)
                {
                    first = glyph.Ascii;
                }

                last = glyph.Ascii;
                yAdvance = Math.Max(yAdvance, glyph.Height + 1);

                int bitmapOffset = Bitmaps.Count;
                ExportGlyphRecord record = new ExportGlyphRecord
                {
                    Glyph = glyph,
                    BitmapOffset = bitmapOffset
                };
                Addresses.Add(record);

                int bitmapSizeInBites = (glyph.Width * glyph.Height - 1) / (int)parameters[0] + 1;
                byte[] bitmap = new byte[bitmapSizeInBites];

                foreach (Rectangle glyphCanvasPixel in glyph.CanvasPixels)
                {
                    int x = (int)Canvas.GetLeft(glyphCanvasPixel) / 20;
                    int y = (int)Canvas.GetTop(glyphCanvasPixel) / 20;

                    if (x >= glyph.Width || y >= glyph.Height)
                    {
                        continue;
                    }

                    SolidColorBrush brush = glyphCanvasPixel.Fill as SolidColorBrush;
                    byte value = 0;
                    if (brush != null)
                    {
                        value = brush.Color == Colors.Red ? (byte)0b01 : brush.Color == Colors.Green ? (byte)0b11 : (byte)0b00;
                        value &= (byte)(int)parameters[2];
                    }

                    int pixelAddress = x + y * glyph.Width;
                    int byteAddresss = pixelAddress / (int)parameters[0];
                    int bitAddress = (int)parameters[0] - 1 - pixelAddress % (int)parameters[0];

                    bitmap[byteAddresss] |= (byte)(value << (bitAddress * (8 / (int)parameters[0])));
                }

                Bitmaps.AddRange(bitmap);
            }

            // Remove last set of empty glyphs
            while (Addresses[Addresses.Count - 1].BitmapOffset == 0)
            {
                Addresses.RemoveAt(Addresses.Count - 1);
            }

            // Write Bitmaps
            writer.WriteLine($"const uint8_t {Filename}Bitmaps[] PROGMEM =");
            writer.Write("{");
            int breakCounter = 0;
            for (int idx = 0; idx < Bitmaps.Count; idx++)
            {
                if (breakCounter++ % 12 == 0)
                {
                    // Break a new line every 12 bytes written
                    writer.WriteLine();
                    writer.Write("  ");
                }

                string hex = $"{Bitmaps[idx]:X}".PadLeft(2, '0');
                writer.Write($"0x{hex}");
                if (idx != Bitmaps.Count - 1)
                {
                    writer.Write(", ");
                }
            }

            writer.WriteLine();
            writer.WriteLine("};");

            // Write Array of glyphs
            writer.WriteLine();
            writer.WriteLine($"const {parameters[1]}glyph {Filename}Glyphs[] PROGMEM =");
            writer.WriteLine("{");
            for (int idx = 0; idx < Addresses.Count; idx++)
            {
                Glyph glyph = Addresses[idx].Glyph;

                writer.Write("  { ");
                writer.Write(Addresses[idx].BitmapOffset);
                writer.Write(", ");
                writer.Write(glyph.Width);
                writer.Write(", ");
                writer.Write(glyph.Height);
                writer.Write(", ");
                writer.Write(glyph.AdvanceX);
                writer.Write(", ");
                writer.Write(glyph.OffsetX);
                writer.Write(", ");
                writer.Write(glyph.OffsetY);
                writer.Write(" }");
                if (idx != Addresses.Count - 1)
                {
                    writer.Write(",");
                }

                writer.Write($"  // 0x{glyph.Ascii:X} ");
                writer.Write($"'{glyph.Name}'");
                writer.WriteLine();
            }

            writer.WriteLine("};");

            // Write font
            writer.WriteLine();
            writer.WriteLine($"const {parameters[1]}font {Filename} PROGMEM =");
            writer.WriteLine("{");
            writer.WriteLine($"  (uint8_t*) {Filename}Bitmaps,");
            writer.WriteLine($"  ({parameters[1]}glyph*) {Filename}Glyphs,");
            writer.WriteLine($"  0x{first:X}, 0x{last:X}, {yAdvance}");
            writer.WriteLine("};");
            writer.WriteLine($"\r\n// Approx. {Bitmaps.Count} bytes");
        }
    }
}
