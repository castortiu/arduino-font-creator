﻿//-----------------------------------------------------------------------
// <copyright file="GlyphsManager.cs" company="Castores Inc.">
//     Custom company copyright tag.
// </copyright>
//-----------------------------------------------------------------------
namespace FontCreator
{
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.ComponentModel;
    using System.IO;
    using System.Text;
    using System.Windows;
    using Newtonsoft.Json;

    /// <summary>
    /// GlyphsManager class
    /// </summary>
    /// <seealso cref="System.ComponentModel.INotifyPropertyChanged" />
    public class GlyphsManager : INotifyPropertyChanged
    {
        /// <summary>
        /// The glyphs names
        /// </summary>
        private static readonly Dictionary<byte, string> GlyphsNames = new Dictionary<byte, string>
        {
            { 0, "(NULL)" }, { 1, "(SOH)" }, { 2, "(STX)" }, { 3, "(ETX)" },
            { 4, "(EOT)" }, { 5, "(ENQ)" }, { 6, "(ACK)" }, { 7, "(BEL)" },
            { 8, "(BEL)" }, { 9, "(HT)" }, { 10, "(LF)" }, { 11, "(VT)" },
            { 12, "(FF)" }, { 13, "(CR)" }, { 14, "(SO)" }, { 15, "(SI)" },
            { 16, "(DLE)" }, { 17, "(DC1)" }, { 18, "(DC2)" }, { 19, "(DC3)" },
            { 20, "(DC4)" }, { 21, "(NAK)" }, { 22, "(SYN)" }, { 23, "(ETB)" },
            { 24, "(CAN)" }, { 25, "(EM)" }, { 26, "(SUB)" }, { 27, "(ESC)" },
            { 28, "(FS)" }, { 29, "(GS)" }, { 30, "(RS)" }, { 31, "(US)" },
            { 127, "(DEL)" }
        };

        /// <summary>
        /// The file path
        /// </summary>
        private string filePath;

        /// <summary>
        /// The modified
        /// </summary>
        private bool modified;

        /// <summary>
        /// Initializes a new instance of the <see cref="GlyphsManager"/> class.
        /// </summary>
        public GlyphsManager()
        {
            Glyphs = new ObservableCollection<Glyph>(CreateDefaultGlyph());
        }

        /// <summary>
        /// Occurs when a property value changes.
        /// </summary>
        public event PropertyChangedEventHandler PropertyChanged;

        /// <summary>
        /// Gets the glyphs.
        /// </summary>
        public ObservableCollection<Glyph> Glyphs { get; }

        /// <summary>
        /// Gets the name of the file.
        /// </summary>
        public string FileName => Path.GetFileName(filePath);

        /// <summary>
        /// Gets or sets the file path.
        /// </summary>
        public string FilePath
        {
            get => filePath;
            set
            {
                filePath = value;
                OnPropertyChanged("FilePath");
                OnPropertyChanged("FileName");
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether this <see cref="GlyphsManager"/> is modified.
        /// </summary>
        public bool Modified
        {
            get => modified;
            set
            {
                modified = value;
                OnPropertyChanged("Modified");
            }
        }

        /// <summary>
        /// Loads the file.
        /// </summary>
        /// <param name="fileName">Name of the file.</param>
        /// <returns>True if the file was loaded successfully</returns>
        public bool LoadFile(string fileName)
        {
            try
            {
                using (TextReader reader = new StreamReader(fileName))
                {
                    string file = reader.ReadToEnd();
                    Glyphs.Clear();
                    JsonConvert.PopulateObject(file, Glyphs);
                    foreach (Glyph glyph in Glyphs)
                    {
                        glyph.Modified = false;
                        glyph.Manager = this;
                    }

                    Modified = false;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error loading the font\r\n\r\n" + ex.Message, "Arduino Font Creator", MessageBoxButton.OK, MessageBoxImage.Error);
                return false;
            }

            FilePath = fileName;
            return true;
        }

        /// <summary>
        /// News this instance.
        /// </summary>
        public void New()
        {
            Glyphs.Clear();
            foreach (Glyph glymp in CreateDefaultGlyph())
            {
                Glyphs.Add(glymp);
            }

            filePath = null;
            Modified = false;
        }

        /// <summary>
        /// Saves this instance.
        /// </summary>
        /// <returns>True if the file was saved successfully</returns>
        public bool Save()
        {
            if (filePath == null)
            {
                return false;
            }

            using (TextWriter writer = new StreamWriter(filePath, false))
            {
                string glyphsCollection = JsonConvert.SerializeObject(Glyphs);
                writer.Write(glyphsCollection);
            }

            foreach (Glyph glymp in Glyphs)
            {
                glymp.Modified = false;
            }

            Modified = false;
            return true;
        }

        /// <summary>
        /// Exports the specified file name.
        /// </summary>
        /// <param name="fileName">Name of the file.</param>
        /// <param name="exportType">Type of the export.</param>
        /// <param name="legacy">if set to <c>true</c> [legacy].</param>
        public void Export(string fileName, ExportType exportType, bool legacy)
        {
            IExport exportInstance;
            switch (exportType)
            {
                case ExportType.Esp8266:
                {
                    exportInstance = new ExportEsp8266();
                    break;
                }

                default:
                {
                    MessageBox.Show($"Export type '{exportType.ToString()}' is not supported", "Arduino Font Export", MessageBoxButton.OK, MessageBoxImage.Information);
                    return;
                }
            }

            try
            {
                if (exportInstance.Export(fileName, Glyphs, legacy) == false)
                {
                    MessageBox.Show("Failed to export font\r\n\r\n", "Arduino Font Export", MessageBoxButton.OK, MessageBoxImage.Error);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Failed to export font\r\n\r\n" + ex.Message, "Arduino Font Export", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        /// <summary>
        /// Creates the default glyph.
        /// </summary>
        /// <returns>Array of Glyphs</returns>
        public Glyph[] CreateDefaultGlyph()
        {
            List<Glyph> glyphs = new List<Glyph>();
            for (int idx = 0; idx < 256; idx++)
            {
                if (GlyphsNames.TryGetValue((byte)idx, out var name) == false)
                {
                    Encoding e = Encoding.GetEncoding("Windows-1252");

                    name = e.GetString(new[] { (byte)idx });
                }

                Glyph glyph = new Glyph(this)
                {
                    Ascii = (byte)idx,
                    Name = name,
                    Used = false
                };

                glyphs.Add(glyph);
            }

            return glyphs.ToArray();
        }

        /// <summary>
        /// Called when [property changed].
        /// </summary>
        /// <param name="name">The name.</param>
        protected void OnPropertyChanged(string name)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(name));
        }
    }
}
