﻿//-----------------------------------------------------------------------
// <copyright file="CanvasPixelsConverter.cs" company="Castores Inc.">
//     Custom company copyright tag.
// </copyright>
//-----------------------------------------------------------------------
namespace FontCreator
{
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Linq;
    using System.Windows.Controls;
    using System.Windows.Media;
    using System.Windows.Shapes;
    using Newtonsoft.Json;
    using Newtonsoft.Json.Linq;

    /// <summary>
    /// CanvasPixelsConverter class
    /// </summary>
    /// <seealso cref="Newtonsoft.Json.JsonConverter" />
    public class CanvasPixelsConverter : JsonConverter
    {
        /// <summary>
        /// Gets a value indicating whether this <see cref="T:Newtonsoft.Json.JsonConverter" /> can read JSON.
        /// </summary>
        public override bool CanRead => true;

        /// <summary>
        /// Gets a value indicating whether this <see cref="T:Newtonsoft.Json.JsonConverter" /> can write JSON.
        /// </summary>
        public override bool CanWrite => true;

        /// <summary>
        /// Determines whether this instance can convert the specified object type.
        /// </summary>
        /// <param name="objectType">Type of the object.</param>
        /// <returns>
        /// <c>true</c> if this instance can convert the specified object type; otherwise, <c>false</c>.
        /// </returns>
        public override bool CanConvert(Type objectType)
        {
            return typeof(ObservableCollection<Rectangle>) == objectType;
        }

        /// <summary>
        /// Reads the JSON representation of the object.
        /// </summary>
        /// <param name="reader">The <see cref="T:Newtonsoft.Json.JsonReader" /> to read from.</param>
        /// <param name="objectType">Type of the object.</param>
        /// <param name="existingValue">The existing value of object being read.</param>
        /// <param name="serializer">The calling serializer.</param>
        /// <returns>
        /// The object value.
        /// </returns>
        public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
        {
            ObservableCollection<Rectangle> pixels = new ObservableCollection<Rectangle>();

            if (reader.TokenType != JsonToken.Null &&
                reader.TokenType == JsonToken.StartArray)
            {
                JToken token = JToken.Load(reader);
                List<JSonRec> items = token.ToObject<List<JSonRec>>();

                foreach (JSonRec jsonRec in items)
                {
                    Rectangle rec = new Rectangle();
                    Canvas.SetLeft(rec, jsonRec.X);
                    Canvas.SetTop(rec, jsonRec.Y);
                    rec.StrokeThickness = 2;
                    rec.Stroke = new SolidColorBrush(Colors.Black);
                    rec.Width = jsonRec.Width;
                    rec.Height = jsonRec.Height;
                    rec.Fill = new SolidColorBrush(jsonRec.Color);
                    pixels.Add(rec);
                }
            }

            return pixels;
        }

        /// <summary>
        /// Writes the JSON representation of the object.
        /// </summary>
        /// <param name="writer">The <see cref="T:Newtonsoft.Json.JsonWriter" /> to write to.</param>
        /// <param name="value">The value.</param>
        /// <param name="serializer">The calling serializer.</param>
        public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
        {
            writer.WriteStartArray();
            IEnumerable<JSonRec> recs = ((ObservableCollection<Rectangle>)value).Select(_ =>
                new JSonRec
                {
                    X = (int)Canvas.GetLeft(_),
                    Y = (int)Canvas.GetTop(_),
                    Width = (int)_.Width,
                    Height = (int)_.Height,
                    Color = ((SolidColorBrush)_.Fill).Color
                });

            foreach (JSonRec rec in recs)
            {
                string stringRec = JsonConvert.SerializeObject(rec);
                writer.WriteRawValue(stringRec);
            }

            writer.WriteEndArray();
        }

        /// <summary>
        /// JSonRec class
        /// </summary>
        private class JSonRec
        {
            /// <summary>
            /// Gets or sets the x.
            /// </summary>
            public int X { get; set; }

            /// <summary>
            /// Gets or sets the y.
            /// </summary>
            public int Y { get; set; }

            /// <summary>
            /// Gets or sets the width.
            /// </summary>
            public int Width { get; set; }

            /// <summary>
            /// Gets or sets the height.
            /// </summary>
            public int Height { get; set; }

            /// <summary>
            /// Gets or sets the color.
            /// </summary>
            public Color Color { get; set; }
        }
    }
}
