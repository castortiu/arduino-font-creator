﻿//-----------------------------------------------------------------------
// <copyright file="MainWindow.xaml.cs" company="Castores Inc.">
//     Custom company copyright tag.
// </copyright>
//-----------------------------------------------------------------------
namespace FontCreator
{
    using System.Collections.ObjectModel;
    using System.ComponentModel;
    using System.Reflection;
    using System.Windows;
    using System.Windows.Controls;
    using System.Windows.Input;
    using System.Windows.Media;
    using System.Windows.Shapes;
    using Microsoft.Win32;

    /// <summary>
    /// MainWindow Class
    /// </summary>
    public partial class MainWindow : INotifyPropertyChanged
    {
        /// <summary>
        /// The manager
        /// </summary>
        private readonly GlyphsManager manager = new GlyphsManager();

        /// <summary>
        /// The selected glyph
        /// </summary>
        private Glyph selectedGlyph;

        /// <summary>
        /// The preview zoom
        /// </summary>
        private int previewZoom = 1;

        /// <summary>
        /// The modified
        /// </summary>
        private bool modified;

        /// <summary>
        /// Initializes a new instance of the <see cref="MainWindow"/> class.
        /// </summary>
        public MainWindow()
        {
            InitializeComponent();
            CanvasPixels = new ObservableCollection<Rectangle>();
            manager.PropertyChanged += Manager_PropertyChanged;
            manager.Modified = false;
            UpdateFileName();
        }

        /// <summary>
        /// Occurs when a property value changes.
        /// </summary>
        public event PropertyChangedEventHandler PropertyChanged;

        /// <summary>
        /// Gets or sets the name of the file.
        /// </summary>
        public string FileName { get; set; }

        /// <summary>
        /// Gets the glyphs.
        /// </summary>
        public ObservableCollection<Glyph> Glyphs => manager.Glyphs;

        /// <summary>
        /// Gets or sets a value indicating whether this <see cref="MainWindow"/> is modified.
        /// </summary>
        public bool Modified
        {
            get => modified;
            set
            {
                modified = value;
                OnPropertyChanged("Modified");
            }
        }

        /// <summary>
        /// Gets or sets the selected glyph.
        /// </summary>
        public Glyph SelectdGlyph
        {
            get => selectedGlyph;
            set
            {
                selectedGlyph = value;
                GlyphPreviewBorder.BorderBrush = selectedGlyph?.Image != null ? new SolidColorBrush(Colors.Blue) : new SolidColorBrush(Colors.Transparent);
                CanvasPixels = selectedGlyph?.CanvasPixels;
                OnPropertyChanged("CanvasPixels");
                OnPropertyChanged("SelectdGlyph");
            }
        }

        /// <summary>
        /// Gets the canvas pixels.
        /// </summary>
        public ObservableCollection<Rectangle> CanvasPixels { get; private set; }

        /// <summary>
        /// Called when [property changed].
        /// </summary>
        /// <param name="name">The name.</param>
        protected void OnPropertyChanged(string name)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(name));
        }

        /// <summary>
        /// Handles the PropertyChanged event of the Manager control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="PropertyChangedEventArgs"/> instance containing the event data.</param>
        private void Manager_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            if (e.PropertyName == "FileName" || e.PropertyName == "Modified")
            {
                UpdateFileName();
            }

            if (e.PropertyName == "Modified")
            {
                Modified = manager.Modified;
            }
        }

        /// <summary>
        /// Updates the name of the file.
        /// </summary>
        private void UpdateFileName()
        {
            FileName = System.IO.Path.GetFileNameWithoutExtension(manager.FileName) ?? "(Untitled)";
            MainWindowInstance.Title = FileName + (manager.Modified ? "*" : string.Empty) + " - " + Assembly.GetEntryAssembly().GetName().Name;
            OnPropertyChanged("FileName");
        }

        /// <summary>
        /// Handles the Click event of the MenuItem_Open control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="RoutedEventArgs"/> instance containing the event data.</param>
        private void MenuItem_Open_Click(object sender, RoutedEventArgs e)
        {
            OpenFileDialog fileDialog = new OpenFileDialog
            {
                DefaultExt = ".xft",
                Filter = "Extended Arduino Font|*.xft"
            };

            if (fileDialog.ShowDialog() == true)
            {
                manager.LoadFile(fileDialog.FileName);
            }
        }

        /// <summary>
        /// Handles the Click event of the MenuItem_New control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="RoutedEventArgs"/> instance containing the event data.</param>
        private void MenuItem_New_Click(object sender, RoutedEventArgs e)
        {
            if (manager.Modified)
            {
                MessageBoxResult result = MessageBox.Show($"Do you want to save changes to {FileName}?", Assembly.GetEntryAssembly().GetName().Name, MessageBoxButton.YesNoCancel, MessageBoxImage.Question, MessageBoxResult.OK);
                if (result == MessageBoxResult.Yes)
                {
                    SaveClick(manager.FileName);
                }
                else if (result == MessageBoxResult.Cancel)
                {
                    return;
                }
            }

            manager.New();
        }

        /// <summary>
        /// Handles the Click event of the MenuItem_Save control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="RoutedEventArgs"/> instance containing the event data.</param>
        private void MenuItem_Save_Click(object sender, RoutedEventArgs e)
        {
            SaveClick(manager.FileName);
        }

        /// <summary>
        /// Handles the Click event of the MenuItem_SaveAs control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="RoutedEventArgs"/> instance containing the event data.</param>
        private void MenuItem_SaveAs_Click(object sender, RoutedEventArgs e)
        {
            SaveClick(null);
        }

        /// <summary>
        /// Saves the click.
        /// </summary>
        /// <param name="fileName">Name of the file.</param>
        /// <returns>True if the save was successful</returns>
        private bool SaveClick(string fileName)
        {
            if (fileName == null)
            {
                SaveFileDialog saveFileDialog = new SaveFileDialog
                {
                    AddExtension = true,
                    CheckPathExists = true,
                    DefaultExt = ".xft",
                    Filter = "Extended Arduino Font|*.xft",
                    OverwritePrompt = true
                };

                if (saveFileDialog.ShowDialog() == true)
                {
                    manager.FilePath = saveFileDialog.FileName;
                }
            }

            return manager.Save();
        }

        /// <summary>
        /// Handles the Click event of the MenuItem_Export_ESP8266 control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="RoutedEventArgs"/> instance containing the event data.</param>
        private void MenuItem_Export_ESP8266_Click(object sender, RoutedEventArgs e)
        {
            Export(ExportType.Esp8266, false);
        }

        /// <summary>
        /// Handles the Click event of the MenuItem_Export_ESP8266 control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="RoutedEventArgs"/> instance containing the event data.</param>
        private void MenuItem_Export_ESP8266Adafruit_Click(object sender, RoutedEventArgs e)
        {
            Export(ExportType.Esp8266, true);
        }

        /// <summary>
        /// Exports the specified export type.
        /// </summary>
        /// <param name="exportType">Type of the export.</param>
        /// <param name="legacy">if set to <c>true</c> [legacy].</param>
        private void Export(ExportType exportType, bool legacy)
        {
            string filePath = manager?.FilePath;
            string fileName = System.IO.Path.GetFileNameWithoutExtension(filePath);

            if (fileName != null && filePath != null)
            {
                string directoryName = System.IO.Path.GetDirectoryName(filePath);

                if (directoryName != null)
                {
                    fileName = fileName + ".h";
                }
            }
            else
            {
                MessageBox.Show(
                    "Open a existing file or save the current first export...",
                    "Arduino Font Export", 
                    MessageBoxButton.OK, 
                    MessageBoxImage.Information);
                return;
            }

            SaveFileDialog saveFileDialog = new SaveFileDialog
            {
                Title = "Export",
                FileName = fileName,
                AddExtension = true,
                CheckPathExists = true,
                DefaultExt = ".h",
                Filter = "Header File|*.h",
                OverwritePrompt = true
            };

            if (saveFileDialog.ShowDialog() == true)
            {
                manager.Export(saveFileDialog.FileName, exportType, legacy);
            }
        }

        /// <summary>
        /// Handles the Click event of the MenuItem_Exit control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="RoutedEventArgs"/> instance containing the event data.</param>
        private void MenuItem_Exit_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }

        /// <summary>
        /// Handles the Closing event of the MainWindowDialog control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="CancelEventArgs"/> instance containing the event data.</param>
        private void MainWindowDialog_Closing(object sender, CancelEventArgs e)
        {
            if (manager.Modified)
            {
                MessageBoxResult result = MessageBox.Show($"Do you want to save changes to {FileName}?", Assembly.GetEntryAssembly().GetName().Name, MessageBoxButton.YesNoCancel, MessageBoxImage.Question, MessageBoxResult.OK);
                if (result == MessageBoxResult.Yes)
                {
                    if (SaveClick(manager.FileName))
                    {
                        return;
                    }
                }

                if (result == MessageBoxResult.No)
                {
                    return;
                }

                e.Cancel = true;
            }
        }

        /// <summary>
        /// Handles the SelectionChanged event of the List Glyph control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="SelectionChangedEventArgs"/> instance containing the event data.</param>
        private void LstGlyph_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            SelectdGlyph = e.AddedItems.Count > 0 ? (Glyph)e.AddedItems[0] : null;
        }

        /// <summary>
        /// Handles the MouseWheel event of the GlyphPreview control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="MouseWheelEventArgs"/> instance containing the event data.</param>
        private void GlyphPreview_MouseWheel(object sender, MouseWheelEventArgs e)
        {
            try
            {
                previewZoom += e.Delta / 120;
                if (previewZoom < 1)
                {
                    previewZoom = 1;
                    return;
                }

                if (previewZoom > 10)
                {
                    previewZoom = 10;
                }
            }
            finally
            {
                PreviewScaleTransform.ScaleX = previewZoom;
                PreviewScaleTransform.ScaleY = previewZoom;
            }
        }

        /// <summary>
        /// Handles the MouseLeftButtonUp event of the DrawingCanvas control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="MouseButtonEventArgs"/> instance containing the event data.</param>
        private void DrawingCanvas_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            Point p = e.GetPosition(DrawingCanvasItems);
            selectedGlyph?.SetPixel((int)p.X / 20, (int)p.Y / 20, PixelType.Foreground);
        }

        /// <summary>
        /// Handles the MouseRightButtonUp event of the DrawingCanvas control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="MouseButtonEventArgs"/> instance containing the event data.</param>
        private void DrawingCanvas_MouseRightButtonUp(object sender, MouseButtonEventArgs e)
        {
            Point p = e.GetPosition(DrawingCanvasItems);
            selectedGlyph?.SetPixel((int)p.X / 20, (int)p.Y / 20, PixelType.Background);
        }

        /// <summary>
        /// Handles the Checked event of the Modified control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="RoutedEventArgs"/> instance containing the event data.</param>
        private void Modified_Checked(object sender, RoutedEventArgs e)
        {
            if (SelectdGlyph?.Modified == true)
            {
                manager.Modified = true;
            }
        }
    }
}
