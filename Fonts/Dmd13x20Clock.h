const uint8_t dmd13x20ClockBitmaps[] PROGMEM =
{
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x38, 0xFB, 0xFF, 0xFF, 0xEF, 
  0x8E, 0x00, 0x1F, 0xC1, 0xFF, 0x1F, 0xFD, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 
  0xF7, 0xFF, 0xBF, 0xFD, 0xFF, 0xEF, 0xFF, 0x7F, 0xFB, 0xFF, 0xDF, 0xFE, 
  0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFB, 0xFF, 0x8F, 0xF8, 0x3F, 0x80, 0x0F, 
  0x01, 0xF8, 0x3F, 0x87, 0xF8, 0xFF, 0x8F, 0xF8, 0x7F, 0x81, 0xF8, 0x1F, 
  0x81, 0xF8, 0x1F, 0x81, 0xF8, 0x1F, 0x81, 0xF8, 0x7F, 0xEF, 0xFF, 0xFF, 
  0xFF, 0xFF, 0xFF, 0xF7, 0xFE, 0x1F, 0xC1, 0xFF, 0x1F, 0xFD, 0xFF, 0xFF, 
  0xFF, 0xFF, 0xFF, 0xF7, 0xEF, 0xBF, 0x1F, 0xF9, 0xFF, 0xDF, 0xFD, 0xFF, 
  0xCF, 0xFC, 0x7E, 0x03, 0xFF, 0xDF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFE, 
  0xFF, 0xE0, 0x1F, 0xC1, 0xFF, 0x1F, 0xFD, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 
  0xF7, 0xEF, 0x7F, 0x07, 0xF0, 0x3F, 0x01, 0xFC, 0xF7, 0xFF, 0xDF, 0xFE, 
  0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFB, 0xFF, 0x8F, 0xF8, 0x3F, 0x80, 0x01, 
  0xE0, 0x1F, 0x81, 0xFC, 0x1F, 0xE1, 0xFF, 0x1F, 0xF9, 0xFF, 0xDF, 0xFE, 
  0xFF, 0xF7, 0xFF, 0xFF, 0xFF, 0xFF, 0xF7, 0xFF, 0x01, 0xF8, 0x0F, 0xC0, 
  0x7E, 0x03, 0xF0, 0x1F, 0x80, 0xFC, 0x03, 0xC0, 0x7F, 0xF7, 0xFF, 0xFF, 
  0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFB, 0xF0, 0x1F, 0xF8, 0xFF, 0xE7, 0xFF, 
  0xBF, 0xFE, 0xFF, 0xF7, 0x9F, 0xFE, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFB, 
  0xFF, 0x8F, 0xF8, 0x3F, 0x80, 0x1F, 0xC1, 0xFF, 0x1F, 0xFD, 0xFF, 0xFF, 
  0xFF, 0xFF, 0xFF, 0xF7, 0xFF, 0xFE, 0xFF, 0xE7, 0xFF, 0xBF, 0xFF, 0xFF, 
  0xFF, 0xDF, 0xFE, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFB, 0xFF, 0x8F, 0xF8, 
  0x3F, 0x80, 0x7F, 0xF7, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 
  0xF7, 0xEF, 0x7E, 0x03, 0xF0, 0x3F, 0x01, 0xF8, 0x1F, 0x80, 0xFC, 0x0F, 
  0xC0, 0x7E, 0x07, 0xE0, 0x3F, 0x01, 0xF8, 0x0F, 0xC0, 0x3C, 0x00, 0x1F, 
  0xC1, 0xFF, 0x1F, 0xFD, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xF7, 0xFF, 0xFF, 
  0x7F, 0xF1, 0xFF, 0x1F, 0xFD, 0xFF, 0xFF, 0xDF, 0xFE, 0xFF, 0xFF, 0xFF, 
  0xFF, 0xFF, 0xFB, 0xFF, 0x8F, 0xF8, 0x3F, 0x80, 0x1F, 0xC1, 0xFF, 0x1F, 
  0xFD, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xF7, 0xFF, 0xBF, 0xFF, 0xFB, 0xFF, 
  0xCF, 0xFE, 0x3F, 0xF7, 0xFF, 0xFE, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFB, 
  0xFF, 0x8F, 0xF8, 0x3F, 0x80, 0x77, 0xFF, 0xF7, 0x01, 0xDF, 0xFF, 0xDC, 
  0x3E, 0x3F, 0xBF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xF7, 0xFB, 0xFD, 
  0xEC, 0x60, 0x61, 0xBC, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xCF, 
  0xF3, 0xFC, 0xFF, 0x3D, 0x86, 0x7E, 0x7F, 0xBF, 0xFF, 0xFF, 0xFF, 0xFF, 
  0xFD, 0xFC, 0xF0, 0x78, 0x3C, 0x0C, 0x00, 0x79, 0xFB, 0xE7, 0xCF, 0x1C, 
  0x10, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  0x00, 0x38, 0xFB, 0xE7, 0x0F, 0x8F, 0x8E, 0x00, 0x00, 0x00, 0x00, 0x00, 
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x38, 0xFB, 0xFF, 0x7F, 0xEF, 
  0x8E, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  0x00
};

const GFXglyph dmd13x20ClockGlyphs[] PROGMEM =
{
  { 0, 5, 11, 4, 0, -15 },  // 0x20 ' '
  { 0, 0, 0, 0, 0, 0 },  // 0x21 '!'
  { 0, 0, 0, 0, 0, 0 },  // 0x22 '"'
  { 0, 0, 0, 0, 0, 0 },  // 0x23 '#'
  { 0, 0, 0, 0, 0, 0 },  // 0x24 '$'
  { 0, 0, 0, 0, 0, 0 },  // 0x25 '%'
  { 0, 0, 0, 0, 0, 0 },  // 0x26 '&'
  { 0, 0, 0, 0, 0, 0 },  // 0x27 '''
  { 0, 0, 0, 0, 0, 0 },  // 0x28 '('
  { 0, 0, 0, 0, 0, 0 },  // 0x29 ')'
  { 0, 0, 0, 0, 0, 0 },  // 0x2A '*'
  { 0, 0, 0, 0, 0, 0 },  // 0x2B '+'
  { 0, 0, 0, 0, 0, 0 },  // 0x2C ','
  { 0, 0, 0, 0, 0, 0 },  // 0x2D '-'
  { 7, 7, 7, 6, 0, -7 },  // 0x2E '.'
  { 0, 0, 0, 0, 0, 0 },  // 0x2F '/'
  { 14, 13, 20, 12, 0, -20 },  // 0x30 '0'
  { 47, 12, 20, 11, 0, -20 },  // 0x31 '1'
  { 77, 13, 20, 12, 0, -20 },  // 0x32 '2'
  { 110, 13, 20, 12, 0, -20 },  // 0x33 '3'
  { 143, 13, 20, 12, 0, -20 },  // 0x34 '4'
  { 176, 13, 20, 12, 0, -20 },  // 0x35 '5'
  { 209, 13, 20, 12, 0, -20 },  // 0x36 '6'
  { 242, 13, 20, 12, 0, -20 },  // 0x37 '7'
  { 275, 13, 20, 12, 0, -20 },  // 0x38 '8'
  { 308, 13, 20, 12, 0, -20 },  // 0x39 '9'
  { 341, 5, 11, 4, 0, -15 },  // 0x3A ':'
  { 0, 0, 0, 0, 0, 0 },  // 0x3B ';'
  { 0, 0, 0, 0, 0, 0 },  // 0x3C '<'
  { 0, 0, 0, 0, 0, 0 },  // 0x3D '='
  { 0, 0, 0, 0, 0, 0 },  // 0x3E '>'
  { 0, 0, 0, 0, 0, 0 },  // 0x3F '?'
  { 0, 0, 0, 0, 0, 0 },  // 0x40 '@'
  { 348, 9, 12, 8, 0, -12 },  // 0x41 'A'
  { 0, 0, 0, 0, 0, 0 },  // 0x42 'B'
  { 0, 0, 0, 0, 0, 0 },  // 0x43 'C'
  { 0, 0, 0, 0, 0, 0 },  // 0x44 'D'
  { 0, 0, 0, 0, 0, 0 },  // 0x45 'E'
  { 0, 0, 0, 0, 0, 0 },  // 0x46 'F'
  { 0, 0, 0, 0, 0, 0 },  // 0x47 'G'
  { 0, 0, 0, 0, 0, 0 },  // 0x48 'H'
  { 0, 0, 0, 0, 0, 0 },  // 0x49 'I'
  { 0, 0, 0, 0, 0, 0 },  // 0x4A 'J'
  { 0, 0, 0, 0, 0, 0 },  // 0x4B 'K'
  { 0, 0, 0, 0, 0, 0 },  // 0x4C 'L'
  { 362, 10, 12, 9, 0, -12 },  // 0x4D 'M'
  { 0, 0, 0, 0, 0, 0 },  // 0x4E 'N'
  { 0, 0, 0, 0, 0, 0 },  // 0x4F 'O'
  { 377, 9, 12, 8, 0, -12 },  // 0x50 'P'
  { 0, 0, 0, 0, 0, 0 },  // 0x51 'Q'
  { 0, 0, 0, 0, 0, 0 },  // 0x52 'R'
  { 0, 0, 0, 0, 0, 0 },  // 0x53 'S'
  { 0, 0, 0, 0, 0, 0 },  // 0x54 'T'
  { 0, 0, 0, 0, 0, 0 },  // 0x55 'U'
  { 0, 0, 0, 0, 0, 0 },  // 0x56 'V'
  { 0, 0, 0, 0, 0, 0 },  // 0x57 'W'
  { 0, 0, 0, 0, 0, 0 },  // 0x58 'X'
  { 0, 0, 0, 0, 0, 0 },  // 0x59 'Y'
  { 0, 0, 0, 0, 0, 0 },  // 0x5A 'Z'
  { 0, 0, 0, 0, 0, 0 },  // 0x5B '['
  { 0, 0, 0, 0, 0, 0 },  // 0x5C '\'
  { 0, 0, 0, 0, 0, 0 },  // 0x5D ']'
  { 0, 0, 0, 0, 0, 0 },  // 0x5E '^'
  { 0, 0, 0, 0, 0, 0 },  // 0x5F '_'
  { 0, 0, 0, 0, 0, 0 },  // 0x60 '`'
  { 0, 0, 0, 0, 0, 0 },  // 0x61 'a'
  { 0, 0, 0, 0, 0, 0 },  // 0x62 'b'
  { 0, 0, 0, 0, 0, 0 },  // 0x63 'c'
  { 0, 0, 0, 0, 0, 0 },  // 0x64 'd'
  { 0, 0, 0, 0, 0, 0 },  // 0x65 'e'
  { 0, 0, 0, 0, 0, 0 },  // 0x66 'f'
  { 0, 0, 0, 0, 0, 0 },  // 0x67 'g'
  { 0, 0, 0, 0, 0, 0 },  // 0x68 'h'
  { 0, 0, 0, 0, 0, 0 },  // 0x69 'i'
  { 0, 0, 0, 0, 0, 0 },  // 0x6A 'j'
  { 0, 0, 0, 0, 0, 0 },  // 0x6B 'k'
  { 0, 0, 0, 0, 0, 0 },  // 0x6C 'l'
  { 0, 0, 0, 0, 0, 0 },  // 0x6D 'm'
  { 0, 0, 0, 0, 0, 0 },  // 0x6E 'n'
  { 0, 0, 0, 0, 0, 0 },  // 0x6F 'o'
  { 0, 0, 0, 0, 0, 0 },  // 0x70 'p'
  { 0, 0, 0, 0, 0, 0 },  // 0x71 'q'
  { 0, 0, 0, 0, 0, 0 },  // 0x72 'r'
  { 0, 0, 0, 0, 0, 0 },  // 0x73 's'
  { 0, 0, 0, 0, 0, 0 },  // 0x74 't'
  { 0, 0, 0, 0, 0, 0 },  // 0x75 'u'
  { 0, 0, 0, 0, 0, 0 },  // 0x76 'v'
  { 0, 0, 0, 0, 0, 0 },  // 0x77 'w'
  { 0, 0, 0, 0, 0, 0 },  // 0x78 'x'
  { 0, 0, 0, 0, 0, 0 },  // 0x79 'y'
  { 0, 0, 0, 0, 0, 0 },  // 0x7A 'z'
  { 0, 0, 0, 0, 0, 0 },  // 0x7B '{'
  { 0, 0, 0, 0, 0, 0 },  // 0x7C '|'
  { 0, 0, 0, 0, 0, 0 },  // 0x7D '}'
  { 0, 0, 0, 0, 0, 0 },  // 0x7E '~'
  { 0, 0, 0, 0, 0, 0 },  // 0x7F '(DEL)'
  { 0, 0, 0, 0, 0, 0 },  // 0x80 '€'
  { 0, 0, 0, 0, 0, 0 },  // 0x81 ''
  { 0, 0, 0, 0, 0, 0 },  // 0x82 '‚'
  { 0, 0, 0, 0, 0, 0 },  // 0x83 'ƒ'
  { 0, 0, 0, 0, 0, 0 },  // 0x84 '„'
  { 0, 0, 0, 0, 0, 0 },  // 0x85 '…'
  { 0, 0, 0, 0, 0, 0 },  // 0x86 '†'
  { 0, 0, 0, 0, 0, 0 },  // 0x87 '‡'
  { 0, 0, 0, 0, 0, 0 },  // 0x88 'ˆ'
  { 0, 0, 0, 0, 0, 0 },  // 0x89 '‰'
  { 0, 0, 0, 0, 0, 0 },  // 0x8A 'Š'
  { 0, 0, 0, 0, 0, 0 },  // 0x8B '‹'
  { 0, 0, 0, 0, 0, 0 },  // 0x8C 'Œ'
  { 0, 0, 0, 0, 0, 0 },  // 0x8D ''
  { 0, 0, 0, 0, 0, 0 },  // 0x8E 'Ž'
  { 0, 0, 0, 0, 0, 0 },  // 0x8F ''
  { 0, 0, 0, 0, 0, 0 },  // 0x90 ''
  { 0, 0, 0, 0, 0, 0 },  // 0x91 '‘'
  { 0, 0, 0, 0, 0, 0 },  // 0x92 '’'
  { 0, 0, 0, 0, 0, 0 },  // 0x93 '“'
  { 0, 0, 0, 0, 0, 0 },  // 0x94 '”'
  { 0, 0, 0, 0, 0, 0 },  // 0x95 '•'
  { 0, 0, 0, 0, 0, 0 },  // 0x96 '–'
  { 0, 0, 0, 0, 0, 0 },  // 0x97 '—'
  { 0, 0, 0, 0, 0, 0 },  // 0x98 '˜'
  { 0, 0, 0, 0, 0, 0 },  // 0x99 '™'
  { 0, 0, 0, 0, 0, 0 },  // 0x9A 'š'
  { 0, 0, 0, 0, 0, 0 },  // 0x9B '›'
  { 0, 0, 0, 0, 0, 0 },  // 0x9C 'œ'
  { 0, 0, 0, 0, 0, 0 },  // 0x9D ''
  { 0, 0, 0, 0, 0, 0 },  // 0x9E 'ž'
  { 0, 0, 0, 0, 0, 0 },  // 0x9F 'Ÿ'
  { 0, 0, 0, 0, 0, 0 },  // 0xA0 ' '
  { 0, 0, 0, 0, 0, 0 },  // 0xA1 '¡'
  { 0, 0, 0, 0, 0, 0 },  // 0xA2 '¢'
  { 0, 0, 0, 0, 0, 0 },  // 0xA3 '£'
  { 0, 0, 0, 0, 0, 0 },  // 0xA4 '¤'
  { 0, 0, 0, 0, 0, 0 },  // 0xA5 '¥'
  { 0, 0, 0, 0, 0, 0 },  // 0xA6 '¦'
  { 0, 0, 0, 0, 0, 0 },  // 0xA7 '§'
  { 0, 0, 0, 0, 0, 0 },  // 0xA8 '¨'
  { 0, 0, 0, 0, 0, 0 },  // 0xA9 '©'
  { 391, 7, 20, 6, 0, -20 },  // 0xAA 'ª'
  { 409, 7, 20, 6, 0, -20 },  // 0xAB '«'
  { 0, 0, 0, 0, 0, 0 },  // 0xAC '¬'
  { 0, 0, 0, 0, 0, 0 },  // 0xAD '­'
  { 0, 0, 0, 0, 0, 0 },  // 0xAE '®'
  { 0, 0, 0, 0, 0, 0 },  // 0xAF '¯'
  { 427, 7, 20, 6, 0, -20 }  // 0xB0 '°'
};

const GFXfont dmd13x20Clock PROGMEM =
{
  (uint8_t*) dmd13x20ClockBitmaps,
  (GFXglyph*) dmd13x20ClockGlyphs,
  0x20, 0xB0, 21
};

// Approx. 445 bytes
