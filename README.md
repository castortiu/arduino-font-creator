# Arduino Font Creator #

This is a tool to create bitmap fonts for Arduino.

Arduino Font Creator create bitmap with extended attributes, these attributes allow the rendering
of the font to support borders and alpha blending. 

At this time, the library support export to Extended and also to well-known GFX Adafruit fonts.

Fonts exported as GFX lose all extended attributes and work as a regular font for the Adafruit library.

# Properties #
### Width: ###
This is the width of the Glyph, this value has a direct implication on the .h size since is the number of horizontal pixels that will be rendered on the screen, empty pixels also take space.

### Height: ###
This is the height of the Glyph, this value has a direct implication on the .h size since is the number of vertical pixels that will be rendered on the screen, empty pixels also take space.

### AdvanceX: ###
Number of pixels that the rendering engine will move right from the cursor after the glyph is rendered on the screen, usually this value is Width + 1 since every character should be rendered next each other with one empty pixel between them. For example a value of Width + 5 will leave 5 empty pixels to the next character. 

There are cases where this value should be With -1, When a Glyph has borders (usually black), they take 2 extra pixels on the left/right of the glyph, if the default Width + 1 is used for AdanceX then 3 pixels will be between each characters, so if we want to have only one pixel between them, then we have to advance only Width -1.

### OffsetX: ###
How many pixels this glyph will be offset to the left from the cursor during rendering without affecting the cursor position, usually this value is 0

### OffsetY: ###
How many pixels this glyph will be offset to the top of the cursor during rendering without affecting the cursor position, usually this value is 0, however for example lower case Glyphs may require offset from the cursor position. as for example the glyph "P" OffsetY is 0 however the glyph "p" has a OffsetY of 2 since the bottom portion of the "p" fall under the cursor line.

### Auto generate border: ###
Automatically add and remove a border around the pixel every time a new pixels is added/removed from the glyph (This is not used on Adafruit GFX fonts and borders are ignored during Adafruit exports)

### Left-Click: ###
Add/Toggle a foreground pixel or remove it if a foreground pixel was already set

### Right-Click: ###
Add/Toggle a border pixel or remove it if a border pixel was already set

### Mouse-Wheel on preview: ###
Zoom-In, Zoom-Out the preview

# Binaries #

* [Version 1.0.0 - 12/10/2017](https://bitbucket.org/castortiu/arduino-font-creator/downloads)

# Screenshot #
![Scheme](Resources/Screenshot-1.png)

                                                       